<?php

/**
 * @file
 * listkeeper.form.add.tpl.php
 */

  global $base_url;
?>

  <style type="text/css">

<?php
  for ($cntr=0; $cntr < $form['numberofitems']['#value']; $cntr++) {
?>

    #edit-id<?php print $cntr; ?>-wrapper {
      margin-top:0em;
      margin-bottom:0em;
      font-size:10pt;
    }

    #edit-name<?php print $cntr; ?>-wrapper {
      margin-top:0em;
      margin-bottom:0em;
      font-size:10pt;
    }

    #edit-value<?php print $cntr; ?>-wrapper {
      margin-top:0em;
      margin-bottom:0em;
      font-size:10pt;
    }

    #edit-fnvalue<?php print $cntr; ?>-wrapper {
      margin-top:0em;
      margin-bottom:0em;
      font-size:10pt;
    }

    #edit-isfunction<?php print $cntr; ?>-wrapper {
      margin-top:0em;
      margin-bottom:0em;
      font-size:10pt;
    }

<?php
  }
?>

  </style>

  <div><?php print drupal_render($form['list_name']); ?></div>
  <table style="border: solid black 1px">
    <tr>
      <th>Remove</th>
      <th>ID</th>
      <th>Field Name</th>
      <th>Field Type</th>
      <th>Default Value</th>
      <th>List</th>
      <th>Field</th>
    </tr>

<?php
    $hidescript = '';
    for ($cntr = 0; $cntr < $form['numberofitems']['#value']; $cntr++) {
?>

      <tr>
        <td style="width: 13%"><?php print drupal_render($form['removefield' . $cntr]); ?></td>
        <td style="width: 12%"><?php print $form['id' . $cntr]['#value']; ?></td>
        <td style="width: 25%"><?php print drupal_render($form['name' . $cntr]); ?></td>
        <td style="width: 25%"><?php print drupal_render($form['fnvalue' . $cntr]); ?></td>
        <td style="width: 25%"><?php print drupal_render($form['value' . $cntr]); ?></td>
        <td style="width: 25%"><?php print drupal_render($form['list' . $cntr]); ?></td>
        <td style="width: 25%">
          <div id="replace_fields<?php print $cntr ?>"><?php print drupal_render($form['field' . $cntr]); ?></div>
          <div style="display: none;"><?php print drupal_render($form['isfunction' . $cntr]); ?></div>
        </td>
      </tr>

<?php
      $hidescript .= "hide_applicable_fields('{$form['fnvalue'.$cntr]['#value']}', $cntr);\n";
    }
?>

  </table>

  <script type="text/javascript">
    <?php print $hidescript; ?>
  </script>

  <?php print drupal_render($form['id' . $cntr]); ?>
  <?php print drupal_render($form['addnewfield']); ?>
  <?php print drupal_render($form['submit']); ?>
  <span style="display:">
    <?php print drupal_render($form); ?>
  </span>
