function hide_applicable_fields(value, cntr) {
  var listid = 0;
  var fieldid = 0;

  switch (value) {
  case 'Static Value':
    document.getElementById('edit-isfunction' + cntr).checked = '';
    document.getElementById('edit-list' + cntr).style.display = 'none';
    document.getElementById('edit-field' + cntr).style.display = 'none';
    document.getElementById('edit-value' + cntr).style.display = '';
    document.getElementById('edit-value' + cntr).value = '';
    break;
  case 'listkeeper_get_other_list':
    document.getElementById('edit-isfunction' + cntr).checked = 'checked';
    document.getElementById('edit-list' + cntr).style.display = '';
    document.getElementById('edit-field' + cntr).style.display = '';
    document.getElementById('edit-value' + cntr).style.display = 'none';
    update_value_with_other_list(cntr);
    break;
  default:
    document.getElementById('edit-isfunction' + cntr).checked = 'checked';
    document.getElementById('edit-list' + cntr).style.display = 'none';
    document.getElementById('edit-field' + cntr).style.display = 'none';
    document.getElementById('edit-value' + cntr).style.display = 'none';
    document.getElementById('edit-value' + cntr).value = value;
    break;
  }
}

function update_value_with_other_list(cntr) {
  var listid = document.getElementById('edit-list' + cntr).value;
  var fieldid = document.getElementById('edit-field' + cntr).value;
  document.getElementById('edit-value' + cntr).value = '[list:'+listid+','+fieldid+']';
}
