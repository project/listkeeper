function validate_fields() {
  if (document.getElementById('listkeeper-form-add-values').context.value == 'new') {
    var field;
    var i;
    var validate = true;
    for (i = 0; (field = document.getElementById('edit-n' + i)) != null; i++) {
      if (field.value == '') {
        validate = false;
        break;
      }
    }

    if (!validate) {
      alert('Fill in at least one field!');
    }

    return validate;
  }
  else {
    return true;
  }
}
