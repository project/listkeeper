<?php

/**
 * @file
 * listkeeper.row.tpl.php
 * Default theme implementation to display rows of listkeeper lists
 */
?>

  <table>
  <tr>
      <th>ID</th>
      <th>List Name</th>
      <th>Description</th>
      <th>Actions</th>

  </tr>

<?php
  foreach ($list as $id => $list) {
?>
    <tr>
    <td><?php print $list->id; ?></td>
    <td><?php print $list->name; ?></td>
    <td><?php print $list->description; ?></td>
    <td>
      <a href="?q=admin/settings/listkeeper/edit/<?php print $list->id; ?>">[Edit Definition]</a>
      <a href="?q=admin/settings/listkeeper/values/<?php print $list->id; ?>">[Edit Values]</a>
      <a href="?q=admin/settings/listkeeper/copy/<?php print $list->id; ?>">[Copy]</a>
      <a href="?q=admin/settings/listkeeper/delete/<?php print $list->id; ?>">[Delete]</a>
    </td>
    </tr>
<?php
  }
?>
  </table>
  </tr>
