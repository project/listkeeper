<?php

/**
 * @file
 * custom.php
 */

function listkeeper_get_users($mode='', $val, $maxnumber=0) {
  if ($mode == 'edit') {  //returning an array of values
    $sql = "SELECT uid, name FROM {users} ";
    if ($maxnumber != 0) {
      $sql .= "LIMIT %d";
      $res = db_query($sql, $maxnumber);
    }
    else {
      $res = db_query($sql);
    }

    $retarray = array();
    while ($A = db_fetch_array($res)) {
      $retarray[$A['uid']] = $A['name'];
    }
    return $retarray;
  }
  else {  //returning the result
    $sql = "SELECT name FROM {users} WHERE uid=%d";
    $res = db_query($sql, $val);
    $A = db_fetch_array($res);
    return $A['name'];
  }
}


function listkeeper_get_other_list($mode='', $val, $maxnumber=0) {

}
