<?php

/**
 * @file
 * listkeeper.form.add.values.tpl.php
 */
?>

  <table border="1">
  <tr>
    <th>ID</th>
    <th>Item Order</th>
<?php
    foreach ($fieldarray as $f) {
?>
      <th><?php print $f ?></th>
<?php
    }
?>
    <th>Actions</th>
  </tr>
  <tr>
    <td></td>
    <td><?php print drupal_render($form['nitemorder']);  ?></td>
<?php
    for ($cntr=0; $cntr < count($fieldarray); $cntr++) {
?>
      <td><?php print drupal_render($form['n' . $cntr]);  ?></td>
<?php
    }
?>
    <td><?php print drupal_render($form['submitNewItem']); ?></td>
  </tr>

<?php
  //$resultset stores our overall display data...
  while ($A = db_fetch_array($resultset)) {
    if ($A['id'] == $edit_id) {
      //we have to display the form elements here
?>
      <tr>
        <td><?php print drupal_render($form['id']);?></td>
        <td><?php print drupal_render($form['itemorder']);?></td>
<?php
        for ($cntr = 0; $cntr < count($fieldarray); $cntr++) {
?>
          <td><?php print drupal_render($form['v' . $cntr]);?></td>
<?php
        }
?>
        <td><?php print drupal_render($form['submit']);?></td>
      </tr>
<?php
    }
    else {
?>
      <tr>
        <td><?php print $A['id']; ?></td>
        <td><?php print $A['itemorder']; ?></td>
<?php
        //value display
        $arr = explode(',', $A['value']);
        if (is_array($arr)) {
          for ($cntr=0; $cntr < count($arr); $cntr++) {
            if ($functionarray[$cntr][0] == 1) {
?>
              <td>
<?php
              if (strpos($functionarray[$cntr][1], '[list:') !== FALSE) {
                $listvaluesstr = drupal_substr($functionarray[$cntr][1], 6);
                $listvaluesstr = str_replace(']', '', $listvaluesstr);
                $listvalues = explode(',', $listvaluesstr);
                print listkeeper_option_list('view', '', $listvalues[0], $listvalues[1], $arr[$cntr]);
              }
              else {
                print $functionarray[$cntr][1]('view', $arr[$cntr]);
              }
?>
              </td>
<?php
            }
            else {
?>
              <td><?php print $arr[$cntr]; ?></td>
<?php
            }
          }

          if (count($arr) < count($fieldarray)) {
            for ($cntr; $cntr < count($fieldarray); $cntr++) {
?>
             <td> </td>
<?php
            }
          }
      }
      else {
?>
        <td><?php print $A['value'] ?></td>
<?php
      }
?>
      <td>
        <input type="button" onclick="document.location='<?php print url('admin/settings/listkeeper/values') . "/{$listid}/{$page}/{$A['id']}"; ?>';" value="Edit">
        <input type="button" onclick="document.location='<?php print url('admin/settings/listkeeper/delval') . "/{$listid}/{$A['id']}"; ?>';" value="Delete">
      </td>
    </tr>
<?php
    }
  }
?>
  </table>

  <span style="display:">
    <?php print drupal_render($form); ?>
  </span>
